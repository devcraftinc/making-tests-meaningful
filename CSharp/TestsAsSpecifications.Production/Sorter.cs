﻿// Copyright 2018-2018 DevCraft, Inc.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

using System;
using System.Collections.Generic;

namespace TestsAsSpecifications.Production
{
  public class Sorter
  {
    private class Pair : IComparable<Pair>
    {
      public Pair(string key, int value)
      {
        Key = key;
        Value = value;
      }

      public string Key { get; }
      public int Value { get; }

      public int CompareTo(Pair other)
      {
        var mineHasNumber = HasNumber(Key);
        var otherHasNumber = HasNumber(other.Key);

        if (mineHasNumber == otherHasNumber)
          return string.Compare(Key, other.Key, StringComparison.OrdinalIgnoreCase);

        return mineHasNumber ? -1 : 1;
      }

      private static bool HasNumber(string key)
      {
        foreach (var c in key)
          if (char.IsDigit(c))
            return true;

        return false;
      }
    }

    public static void Apply(List<string> keys, List<int> codes)
    {
      var fixedPosition = new List<int?>();
      var pairs = new List<Pair>();
      for (var i = 0; i < Math.Min(keys.Count, codes.Count); ++i)
      {
        if (keys[i] == "-")
        {
          codes.RemoveAt(i);
          keys.RemoveAt(i);
          i--;
        }
        else if (keys[i] == "+")
        {
          fixedPosition.Add(codes[i]);
        }
        else
        {
          pairs.Add(new Pair(keys[i], codes[i]));
          fixedPosition.Add(null);
        }
      }

      pairs.Sort();

      int j = 0;
      for (var i = 0; i < Math.Min(keys.Count, codes.Count); ++i)
        codes[i] = fixedPosition[i] ?? pairs[j++].Value;
    }
  }
}