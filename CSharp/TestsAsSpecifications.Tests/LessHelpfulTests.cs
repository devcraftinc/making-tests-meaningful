﻿// Copyright 2018-2018 DevCraft, Inc.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestsAsSpecifications.Production;

namespace TestsAsSpecifications.Tests
{
  [TestClass]
  public class LessHelpfulTests
  {
    [TestMethod]
    public void Sort1()
    {
      List<int> codes = new List<int>();
      codes.Add(41);
      codes.Add(15);
      codes.Add(19);
      codes.Add(23);
      codes.Add(51);
      List<string> keys = new List<string>();
      keys.Add("A");
      keys.Add("B");
      keys.Add("C");
      keys.Add("D");
      keys.Add("E");
      Sorter.Apply(keys, codes);
      Assert.AreEqual(codes.Count, 5);
      Assert.AreEqual(codes[0], 41);
      Assert.AreEqual(codes[1], 15);
      Assert.AreEqual(codes[2], 19);
      Assert.AreEqual(codes[3], 23);
      Assert.AreEqual(codes[4], 51);
    }

    [TestMethod]
    public void Sort2()
    {
      List<int> codes = new List<int>();
      codes.Add(41);
      codes.Add(15);
      codes.Add(19);
      codes.Add(23);
      codes.Add(51);
      List<string> keys = new List<string>();
      keys.Add("A");
      keys.Add("B");
      keys.Add("C");
      keys.Add("D");
      keys.Add("1E");
      Sorter.Apply(keys, codes);
      Assert.AreEqual(codes.Count, 5);
      Assert.AreEqual(codes[0], 51);
      Assert.AreEqual(codes[1], 41);
      Assert.AreEqual(codes[2], 15);
      Assert.AreEqual(codes[3], 19);
      Assert.AreEqual(codes[4], 23);
    }

    [TestMethod]
    public void Sort3()
    {
      List<int> codes = new List<int>();
      codes.Add(41);
      codes.Add(15);
      codes.Add(19);
      codes.Add(23);
      codes.Add(51);
      List<string> keys = new List<string>();
      keys.Add("A");
      keys.Add("B");
      keys.Add("C");
      keys.Add("D");
      keys.Add("E1");
      Sorter.Apply(keys, codes);
      Assert.AreEqual(codes.Count, 5);
      Assert.AreEqual(codes[0], 51);
      Assert.AreEqual(codes[1], 41);
      Assert.AreEqual(codes[2], 15);
      Assert.AreEqual(codes[3], 19);
      Assert.AreEqual(codes[4], 23);
    }

    [TestMethod]
    public void Sort4()
    {
      List<int> codes = new List<int>();
      codes.Add(41);
      codes.Add(15);
      codes.Add(19);
      codes.Add(23);
      codes.Add(51);
      List<string> keys = new List<string>();
      keys.Add("1A");
      keys.Add("B");
      keys.Add("C");
      keys.Add("D");
      keys.Add("E1");
      Sorter.Apply(keys, codes);
      Assert.AreEqual(codes.Count, 5);
      Assert.AreEqual(codes[0], 41);
      Assert.AreEqual(codes[1], 51);
      Assert.AreEqual(codes[2], 15);
      Assert.AreEqual(codes[3], 19);
      Assert.AreEqual(codes[4], 23);
    }

    [TestMethod]
    public void Sort5()
    {
      List<int> codes = new List<int>();
      codes.Add(41);
      codes.Add(15);
      codes.Add(19);
      codes.Add(23);
      codes.Add(51);
      List<string> keys = new List<string>();
      keys.Add("1A");
      keys.Add("B");
      keys.Add("c");
      keys.Add("D");
      keys.Add("E1");
      Sorter.Apply(keys, codes);
      Assert.AreEqual(codes.Count, 5);
      Assert.AreEqual(codes[0], 41);
      Assert.AreEqual(codes[1], 51);
      Assert.AreEqual(codes[2], 15);
      Assert.AreEqual(codes[3], 19);
      Assert.AreEqual(codes[4], 23);
    }

    [TestMethod]
    public void Sort6()
    {
      List<int> codes = new List<int>();
      codes.Add(41);
      codes.Add(15);
      codes.Add(19);
      codes.Add(23);
      codes.Add(51);
      List<string> keys = new List<string>();
      keys.Add("A");
      keys.Add("C");
      keys.Add("B");
      Sorter.Apply(keys, codes);
      Assert.AreEqual(codes.Count, 5);
      Assert.AreEqual(codes[0], 41);
      Assert.AreEqual(codes[1], 19);
      Assert.AreEqual(codes[2], 15);
      Assert.AreEqual(codes[3], 23);
      Assert.AreEqual(codes[4], 51);
    }

    [TestMethod]
    public void Sort7()
    {
      List<int> codes = new List<int>();
      codes.Add(41);
      codes.Add(15);
      codes.Add(19);
      codes.Add(23);
      codes.Add(51);
      List<string> keys = new List<string>();
      keys.Add("A");
      keys.Add("C");
      keys.Add("B");
      keys.Add("D");
      keys.Add("E");
      keys.Add("1f");
      Sorter.Apply(keys, codes);
      Assert.AreEqual(codes.Count, 5);
      Assert.AreEqual(codes[0], 41);
      Assert.AreEqual(codes[1], 19);
      Assert.AreEqual(codes[2], 15);
      Assert.AreEqual(codes[3], 23);
      Assert.AreEqual(codes[4], 51);
    }

    [TestMethod]
    public void Sort8()
    {
      List<int> codes = new List<int>();
      codes.Add(41);
      codes.Add(15);
      codes.Add(19);
      codes.Add(23);
      codes.Add(51);
      List<string> keys = new List<string>();
      keys.Add("C");
      keys.Add("C");
      keys.Add("B");
      keys.Add("A");
      keys.Add("C");
      Sorter.Apply(keys, codes);
      Assert.AreEqual(codes.Count, 5);
      Assert.AreEqual(codes[0], 23);
      Assert.AreEqual(codes[1], 19);
      Assert.AreEqual(codes[2], 41);
      Assert.AreEqual(codes[3], 15);
      Assert.AreEqual(codes[4], 51);
    }

    [TestMethod]
    public void Sort9()
    {
      List<int> codes = new List<int>();
      codes.Add(41);
      codes.Add(15);
      codes.Add(19);
      codes.Add(23);
      codes.Add(51);
      List<string> keys = new List<string>();
      keys.Add("A");
      keys.Add("B");
      keys.Add("-");
      keys.Add("C");
      keys.Add("D");
      Sorter.Apply(keys, codes);
      Assert.AreEqual(codes.Count, 4);
      Assert.AreEqual(codes[0], 41);
      Assert.AreEqual(codes[1], 15);
      Assert.AreEqual(codes[2], 23);
      Assert.AreEqual(codes[3], 51);
    }

    [TestMethod]
    public void Sort10()
    {
      List<int> codes = new List<int>();
      codes.Add(41);
      codes.Add(15);
      codes.Add(19);
      codes.Add(23);
      codes.Add(51);
      List<string> keys = new List<string>();
      keys.Add("B");
      keys.Add("B");
      keys.Add("+");
      keys.Add("A");
      keys.Add("A");
      Sorter.Apply(keys, codes);
      Assert.AreEqual(codes.Count, 5);
      Assert.AreEqual(codes[0], 23);
      Assert.AreEqual(codes[1], 51);
      Assert.AreEqual(codes[2], 19);
      Assert.AreEqual(codes[3], 41);
      Assert.AreEqual(codes[4], 15);
    }
  }
}