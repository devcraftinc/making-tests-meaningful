// Copyright 2018-2018 DevCraft, Inc.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


package katas;

import java.util.ArrayList;
import java.util.Objects;

public class Sorter {
    private static class Pair implements Comparable<Pair> {
        public Pair(String key, int value) {
            _Key = key;
            _Value = value;
        }

        private String _Key;

        public String Key() {
            return _Key;
        }

        private int _Value;

        public int Value() {
            return _Value;
        }

        @Override
        public int compareTo(Pair other) {
            boolean mineHasNumber = HasNumber(Key());
            boolean otherHasNumber = HasNumber(other.Key());

            if (mineHasNumber == otherHasNumber)
                return Key().toUpperCase().compareTo(other.Key().toUpperCase());

            return mineHasNumber ? -1 : 1;
        }

        private static boolean HasNumber(String key) {
            for (char c : key.toCharArray())
                if (Character.isDigit(c))
                    return true;

            return false;
        }
    }

    public static void apply(ArrayList<String> keys, ArrayList<Integer> codes) {
        ArrayList<Integer> fixedPosition = new ArrayList<>();
        ArrayList<Pair> pairs = new ArrayList<Pair>();
        for (int i = 0; i < Math.min(keys.size(), codes.size()); ++i) {
            if (Objects.equals(keys.get(i), "-")) {
                codes.remove(i);
                keys.remove(i);
                i--;
            } else if (Objects.equals(keys.get(i), "+")) {
                fixedPosition.add(codes.get(i));
            } else {
                pairs.add(new Pair(keys.get(i), codes.get(i)));
                fixedPosition.add(null);
            }
        }

        pairs.sort(Pair::compareTo);

        int j = 0;
        for (int i = 0; i < Math.min(keys.size(), codes.size()); ++i)
        {
            Integer value = fixedPosition.get(i);
            if (value == null)
                value = pairs.get(j++).Value();
            codes.set(i, value);
        }
    }
}

