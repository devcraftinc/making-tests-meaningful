// Copyright 2018-2018 DevCraft, Inc.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


package katas;


import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

public class LessHelpfulTests {
    @Test
    public void sort1() {
        ArrayList<Integer> codes = new ArrayList<>();
        codes.add(41);
        codes.add(15);
        codes.add(19);
        codes.add(23);
        codes.add(51);
        ArrayList<String> keys = new ArrayList<>();
        keys.add("A");
        keys.add("B");
        keys.add("C");
        keys.add("D");
        keys.add("E");
        Sorter.apply(keys, codes);
        assertEquals(codes.size(), 5);
        assertEquals((int)codes.get(0), 41);
        assertEquals((int)codes.get(1), 15);
        assertEquals((int)codes.get(2), 19);
        assertEquals((int)codes.get(3), 23);
        assertEquals((int)codes.get(4), 51);
    }

    @Test
    public void sort2() {
        ArrayList<Integer> codes = new ArrayList<>();
        codes.add(41);
        codes.add(15);
        codes.add(19);
        codes.add(23);
        codes.add(51);
        ArrayList<String> keys = new ArrayList<>();
        keys.add("A");
        keys.add("B");
        keys.add("C");
        keys.add("D");
        keys.add("1E");
        Sorter.apply(keys, codes);
        assertEquals(codes.size(), 5);
        assertEquals((int)codes.get(0), 51);
        assertEquals((int)codes.get(1), 41);
        assertEquals((int)codes.get(2), 15);
        assertEquals((int)codes.get(3), 19);
        assertEquals((int)codes.get(4), 23);
    }

    @Test
    public void sort3() {
        ArrayList<Integer> codes = new ArrayList<>();
        codes.add(41);
        codes.add(15);
        codes.add(19);
        codes.add(23);
        codes.add(51);
        ArrayList<String> keys = new ArrayList<>();
        keys.add("A");
        keys.add("B");
        keys.add("C");
        keys.add("D");
        keys.add("E1");
        Sorter.apply(keys, codes);
        assertEquals(codes.size(), 5);
        assertEquals((int)codes.get(0), 51);
        assertEquals((int)codes.get(1), 41);
        assertEquals((int)codes.get(2), 15);
        assertEquals((int)codes.get(3), 19);
        assertEquals((int)codes.get(4), 23);
    }

    @Test
    public void sort4() {
        ArrayList<Integer> codes = new ArrayList<>();
        codes.add(41);
        codes.add(15);
        codes.add(19);
        codes.add(23);
        codes.add(51);
        ArrayList<String> keys = new ArrayList<>();
        keys.add("1A");
        keys.add("B");
        keys.add("C");
        keys.add("D");
        keys.add("E1");
        Sorter.apply(keys, codes);
        assertEquals(codes.size(), 5);
        assertEquals((int)codes.get(0), 41);
        assertEquals((int)codes.get(1), 51);
        assertEquals((int)codes.get(2), 15);
        assertEquals((int)codes.get(3), 19);
        assertEquals((int)codes.get(4), 23);
    }

    @Test
    public void sort5() {
        ArrayList<Integer> codes = new ArrayList<>();
        codes.add(41);
        codes.add(15);
        codes.add(19);
        codes.add(23);
        codes.add(51);
        ArrayList<String> keys = new ArrayList<>();
        keys.add("1A");
        keys.add("B");
        keys.add("c");
        keys.add("D");
        keys.add("E1");
        Sorter.apply(keys, codes);
        assertEquals(codes.size(), 5);
        assertEquals((int)codes.get(0), 41);
        assertEquals((int)codes.get(1), 51);
        assertEquals((int)codes.get(2), 15);
        assertEquals((int)codes.get(3), 19);
        assertEquals((int)codes.get(4), 23);
    }

    @Test
    public void sort6() {
        ArrayList<Integer> codes = new ArrayList<>();
        codes.add(41);
        codes.add(15);
        codes.add(19);
        codes.add(23);
        codes.add(51);
        ArrayList<String> keys = new ArrayList<>();
        keys.add("A");
        keys.add("C");
        keys.add("B");
        Sorter.apply(keys, codes);
        assertEquals(codes.size(), 5);
        assertEquals((int)codes.get(0), 41);
        assertEquals((int)codes.get(1), 19);
        assertEquals((int)codes.get(2), 15);
        assertEquals((int)codes.get(3), 23);
        assertEquals((int)codes.get(4), 51);
    }

    @Test
    public void sort7() {
        ArrayList<Integer> codes = new ArrayList<>();
        codes.add(41);
        codes.add(15);
        codes.add(19);
        codes.add(23);
        codes.add(51);
        ArrayList<String> keys = new ArrayList<>();
        keys.add("A");
        keys.add("C");
        keys.add("B");
        keys.add("D");
        keys.add("E");
        keys.add("1f");
        Sorter.apply(keys, codes);
        assertEquals(codes.size(), 5);
        assertEquals((int)codes.get(0), 41);
        assertEquals((int)codes.get(1), 19);
        assertEquals((int)codes.get(2), 15);
        assertEquals((int)codes.get(3), 23);
        assertEquals((int)codes.get(4), 51);
    }

    @Test
    public void sort8() {
        ArrayList<Integer> codes = new ArrayList<>();
        codes.add(41);
        codes.add(15);
        codes.add(19);
        codes.add(23);
        codes.add(51);
        ArrayList<String> keys = new ArrayList<>();
        keys.add("C");
        keys.add("C");
        keys.add("B");
        keys.add("A");
        keys.add("C");
        Sorter.apply(keys, codes);
        assertEquals(codes.size(), 5);
        assertEquals((int)codes.get(0), 23);
        assertEquals((int)codes.get(1), 19);
        assertEquals((int)codes.get(2), 41);
        assertEquals((int)codes.get(3), 15);
        assertEquals((int)codes.get(4), 51);
    }

    @Test
    public void sort9() {
        ArrayList<Integer> codes = new ArrayList<>();
        codes.add(41);
        codes.add(15);
        codes.add(19);
        codes.add(23);
        codes.add(51);
        ArrayList<String> keys = new ArrayList<>();
        keys.add("A");
        keys.add("B");
        keys.add("-");
        keys.add("C");
        keys.add("D");
        Sorter.apply(keys, codes);
        assertEquals(codes.size(), 4);
        assertEquals((int)codes.get(0), 41);
        assertEquals((int)codes.get(1), 15);
        assertEquals((int)codes.get(2), 23);
        assertEquals((int)codes.get(3), 51);
    }

    @Test
    public void sort10() {
        ArrayList<Integer> codes = new ArrayList<>();
        codes.add(41);
        codes.add(15);
        codes.add(19);
        codes.add(23);
        codes.add(51);
        ArrayList<String> keys = new ArrayList<>();
        keys.add("B");
        keys.add("B");
        keys.add("+");
        keys.add("A");
        keys.add("A");
        Sorter.apply(keys, codes);
        assertEquals(codes.size(), 5);
        assertEquals((int) codes.get(0), 23);
        assertEquals((int) codes.get(1), 51);
        assertEquals((int)codes.get(2), 19);
        assertEquals((int)codes.get(3), 41);
        assertEquals((int)codes.get(4), 15);
    }
}

